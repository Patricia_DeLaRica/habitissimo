# Habitissimo

En este documento se comentan algunos de los detalles que se han tenido en cuenta para la realización de este challenge

###Descomposición del problema en estructuras y algoritmos para reducir su complejidad.
Se ha modularizado el proyecto de la siguiente manera:
  
  1. Validaciones
  2. Módulo de datos y endpoints
  3. Gestión de aspecto visuales

###Estilo de código.
Se ha utilizado un código limpio para facilitar la comprensión y lectura del código.

Aunque en el código pueden encontrarse algunos comentarios, se ha intentado que cada uno de los métodos fuera lo suficientemente descriptivo tanto en su nombre como en su implementación haciendo fácil su lectura y obviando comentarios que pudieran resultar redundantes. 

###Nomenclatura de clases, funciones y variables.
Se ha utilizado una nomenclatura siguiendo el estándar del lenguaje.

###Correcta organización del código en ficheros y directorios.
He organizado el proyecto creando los siguientes grupos:

  * Model. Aquí se encuentran todas las clases que forman el modelo de datos.
  * Controller
  * Services. Contiene las clases que gestionan la llamada a los endpoints y la gestión de la persistencia.
  * Utils. Contiene clases encargadas de las validaciones. 
  
###Gestión de excepciones y edge cases.
Se ha controlado el tema de la conectividad, teniendo en cuenta el estado de la misma para la carga de datos iniciales.

Además se han tenido en cuenta diversas validaciones para controlar la inserción de contenido en el formulario de creación de presupuesto, segun los requisitos del challenge.

###Uso de tests unitarios y baterías de pruebas.
Se han realizado test para la comprobación del buen funcionamiento de los métodos que realizan validaciones y carga de datos desde endpoints y persistencia.

##Decisiones tomadas
Dado el enunciado de la prueba, he querido tomar algunas decisiones:

  1. El lenguaje utilizado ha sido Objective C. 
    Aunque he desarrollado proyectos en Objective C y Swift, decidí realizarlo con este lenguaje ya que es con el que más cómoda me siento y con el que mas experiencia tengo.
    
  2. No se ha utilizado ninguna arquitectura concreta. 
    Al ver que el ejercicio era simple no quería sobrecargar el proyecto y decidí modularizarlo lo más posible para facilitar su comprensión sin saturarlo con demasiadas clases.
    
  3. Con respecto a la carga de datos he querido que se hiciera de manera inicial una carga de algunos datos que van a utilizarse de manera continuada. Por eso, las categorías, subcategorías y ubicaciones, serán cargadas en una base de datos local la primera vez que se ejecute la aplicación (siempre y cuando haya conexión a internet), de esta manera solo se hará la llamada a los endpoints la primera vez. 
    Con esto la eficiencia será mayor ya que no hará falta llamar a los endpoints cada vez que se quiera hacer alguna operación con las entidades que he nombrado antes.

  4. No se ha realizado un coverage muy alto porque simplemente he querido ilustrar el uso de test en aplicaciones iOS.

  