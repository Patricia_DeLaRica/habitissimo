//
//  Validator.m
//  Habitissimo
//
//  Created by Patri on 09/08/2019.
//  Copyright © 2019 patri. All rights reserved.
//

#import "Validator.h"
#import "DataService.h"

@implementation Validator

+ (BOOL)validatePhone:(NSString *)phoneNumber {
    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [phoneTest evaluateWithObject:phoneNumber];
}

+ (BOOL)validateLocation:(NSString *)locationText {
    return [DataService locationExists:locationText];
}

+ (BOOL)validateEmailWithString:(NSString*)email {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+ (BOOL)validateStringsNotEmptyInArray:(NSArray *)stringsArray {
    // Discard empty arrays
    if (!stringsArray || stringsArray.count == 0) return FALSE;
    for (NSString *string in stringsArray) {
        if ([string isEqualToString:@""]) return FALSE;
    }
    return TRUE;
}


@end
