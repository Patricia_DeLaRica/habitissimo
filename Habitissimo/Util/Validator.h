//
//  Validator.h
//  Habitissimo
//
//  Created by Patri on 09/08/2019.
//  Copyright © 2019 patri. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Validator : NSObject

+ (BOOL)validatePhone:(NSString *)phoneNumber;
+ (BOOL)validateLocation:(NSString *)locationText;
+ (BOOL)validateEmailWithString:(NSString*)email;
+ (BOOL)validateStringsNotEmptyInArray:(NSArray *)stringsArray;

@end

NS_ASSUME_NONNULL_END
