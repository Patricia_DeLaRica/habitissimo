//
//  DataService.h
//  Habitissimo
//
//  Created by Patri on 06/08/2019.
//  Copyright © 2019 patri. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

NS_ASSUME_NONNULL_BEGIN

@interface DataService : NSObject

+ (void)loadInitialData:(void(^)(BOOL))resultBlock;
+ (BOOL)isInitialized;

+ (RLMResults *)allCategories;
+ (RLMResults *)allSubcategoriesForCategoryId:(NSString *)idCategory;
+ (RLMResults *)allLocations;

+ (RLMResults *)allRequests;
+ (RLMResults *)allRequestsByName:(NSString *)name;
+ (RLMObject *)saveRequestWithName:(NSString *)requestName
                             email:(NSString *)requestEmail
                             phone:(NSString *)requestPhone
                          category:(NSString *)requestCategory
                          location:(NSString *)requestLocation
                       subcategory:(NSString *)requestSubcategory
                       description:(NSString *)requestDescription;

+ (NSArray *)locationAutocompleteStrings;
+ (BOOL)locationExists:(NSString *)location;
+ (NSString *)locationNameForZip:(NSString *)zip;

@end

NS_ASSUME_NONNULL_END
