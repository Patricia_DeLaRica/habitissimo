//
//  DataService.m
//  Habitissimo
//
//  Created by Patri on 06/08/2019.
//  Copyright © 2019 patri. All rights reserved.
//

#import "DataService.h"
#import "CategoryModel.h"
#import "LocationModel.h"
#import "SubcategoryModel.h"
#import "RequestModel.h"

static NSString * _Nonnull const API_BASE_URL = @"https://api.habitissimo.es/";
static NSString * _Nonnull const DATASERVICE_STATUS_KEY_NAME = @"loadDataOK";

@implementation DataService

+ (void)loadInitialData:(void(^)(BOOL))resultBlock {
    
    // Clear all Realm Objects to ensure a initial clean state
    [[RLMRealm defaultRealm] transactionWithBlock:^{
        [[RLMRealm defaultRealm] deleteObjects:[CategoryModel allObjects]];
        [[RLMRealm defaultRealm] deleteObjects:[SubcategoryModel allObjects]];
        [[RLMRealm defaultRealm] deleteObjects:[LocationModel allObjects]];
    }];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:DATASERVICE_STATUS_KEY_NAME];
    [[NSUserDefaults standardUserDefaults] synchronize];

    // Fetch categories
    if ([[DataService allCategories] count] == 0) {
        if (![DataService fetchAndSaveCategories]) {
            resultBlock(FALSE);
            return;
        }
    }
    
    // Fetch subcategories
    for (CategoryModel *cat in [DataService allCategories]) {
        if ([[DataService allSubcategoriesForCategoryId:cat.idCategory] count] == 0) {
            if (![DataService fetchAndSaveSubCategoriesForCategoryId:cat.idCategory]) {
                resultBlock(FALSE);
                return;
            }
        }
    }
    
    // Fetch locations
    if ([[DataService allLocations] count] == 0) {
        if (![DataService fetchAndSaveLocations]) {
            resultBlock(FALSE);
            return;
        }
    }
    
    // Return Success
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:DATASERVICE_STATUS_KEY_NAME];
    [[NSUserDefaults standardUserDefaults] synchronize];
    resultBlock(TRUE);
}

+ (BOOL)isInitialized {
    return [[NSUserDefaults standardUserDefaults] boolForKey:DATASERVICE_STATUS_KEY_NAME];
}

#pragma mark Fetchers

+ (BOOL)fetchAndSaveCategories {
    NSError *error;
    NSString *url_string = [NSString stringWithFormat:@"%@%@", API_BASE_URL, @"category/list/"];
    NSURL *url = [NSURL URLWithString:url_string];
    NSData *data = [NSData dataWithContentsOfURL:url];
    if (!data) { return FALSE; }
    NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    if (error != nil) {
        return FALSE;
    } else {
        NSLog(@"JSON: %@", json);
        [DataService saveCategories:json];
    }
    return TRUE;
}

+ (BOOL)fetchAndSaveSubCategoriesForCategoryId:(NSString *)idCategory{
    NSError *error;
    NSString *url_string = [NSString stringWithFormat:@"%@%@%@", API_BASE_URL, @"category/list/", idCategory];
    NSURL *url = [NSURL URLWithString:url_string];
    NSData *data = [NSData dataWithContentsOfURL:url];
    if (!data) { return FALSE; }
    NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    if (error != nil) {
        return FALSE;
    } else {
        NSLog(@"JSON: %@", json);
        [DataService saveSubCategories:json];
    }
    return TRUE;
}

+ (BOOL)fetchAndSaveLocations {
    NSError *error;
    NSString *url_string = [NSString stringWithFormat:@"%@%@", API_BASE_URL, @"location/list/"];
    NSURL *url = [NSURL URLWithString:url_string];
    NSData *data = [NSData dataWithContentsOfURL:url];
    if (!data) { return FALSE; }
    NSMutableArray *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    if (error != nil) {
        return FALSE;
    } else {
        NSLog(@"JSON: %@", json);
        [DataService saveLocations:json];
    }
    return TRUE;
}

#pragma mark Getters and save methods

+ (RLMResults *)allCategories {
    return [CategoryModel allObjects];
}

+ (RLMResults *)allSubcategoriesForCategoryId:(NSString *)idCategory {
    return [SubcategoryModel objectsWhere:@"idparent = %@", idCategory];
}

+ (RLMResults *)allLocations {
    return [LocationModel allObjects];
}

+ (RLMResults *)allRequests {
    return [RequestModel allObjects];
}

+ (RLMResults *)allRequestsByName:(NSString *)name {
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"nameRequest CONTAINS %@", name];
    RLMResults<RequestModel *> *requestsByName = [RequestModel objectsWithPredicate:pred];
    return requestsByName;
}

+ (RLMObject *)saveRequestWithName:(NSString *)requestName
                             email:(NSString *)requestEmail
                             phone:(NSString *)requestPhone
                          category:(NSString *)requestCategory
                          location:(NSString *)requestLocation
                       subcategory:(NSString *)requestSubcategory
                       description:(NSString *)requestDescription {
    
    RequestModel *requestRealm = [[RequestModel alloc]init];
    requestRealm.nameRequest = requestName;
    requestRealm.email = requestEmail;
    requestRealm.phone = requestPhone;
    requestRealm.category = requestCategory;
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([requestLocation rangeOfCharacterFromSet:notDigits].location == NSNotFound){
        requestRealm.location = [DataService locationNameForZip:requestLocation];
    } else {
        requestRealm.location = requestLocation;
    }
    requestRealm.subcategory = requestSubcategory;
    requestRealm.descriptionRequest = requestDescription;
    
    // Add to Realm with transaction
    [[RLMRealm defaultRealm] beginWriteTransaction];
    [[RLMRealm defaultRealm] addObject:requestRealm];
    [[RLMRealm defaultRealm] commitWriteTransaction];
    return requestRealm;
}

#pragma mark Location methods

+ (BOOL)locationExists:(NSString *)location {
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"nameLocation == %@ OR zip == %@", location, location];
    RLMResults<LocationModel *> *RLMlocation = [LocationModel objectsWithPredicate:pred];
    return (RLMlocation.count > 0) ? true : false;
}

+ (NSString *)locationNameForZip:(NSString *)zip {
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"zip CONTAINS %@", zip];
    RLMResults<LocationModel *> *location = [LocationModel objectsWithPredicate:pred];
    return [[location objectAtIndex:0] nameLocation];
}

+ (NSArray *)locationAutocompleteStrings {
    NSMutableArray *autocompleteStrings = [[NSMutableArray alloc] init];
    for (LocationModel *loc in [DataService allLocations]) {
        [autocompleteStrings addObject:loc.nameLocation];
        [autocompleteStrings addObject:loc.zip];
    }
    return [NSArray arrayWithArray:autocompleteStrings]; // return as an unmutable for efficiency
}

#pragma mark Realm Storage Methods

+ (void)saveCategories:(NSMutableArray *)json {
    for (NSDictionary * category in json) {
        CategoryModel * categoryRealm = [[CategoryModel alloc] initWithValue:@{
                                                                               @"idCategory" : [category valueForKey:@"id"] ?: @"",
                                                                               @"nameCategory" : [category valueForKey:@"name"] ?: @""
                                                                               }];
        [[RLMRealm defaultRealm] beginWriteTransaction];
        [[RLMRealm defaultRealm] addObject:categoryRealm];
        [[RLMRealm defaultRealm] commitWriteTransaction];

    }
}

+ (void)saveSubCategories:(NSMutableArray *)json {
    for (NSDictionary *subcategory in json) {
        SubcategoryModel *subcategoryRealm = [[SubcategoryModel alloc] initWithValue:@{
                                                                                       @"idSubcategory" : [subcategory valueForKey:@"id"] ?: @"",
                                                                                       @"nameSubcategory" : [subcategory valueForKey:@"name"] ?: @"",
                                                                                       @"idparent" : [subcategory valueForKey:@"parent_id"] ?: @""
                                                                                       }];
        [[RLMRealm defaultRealm] transactionWithBlock:^{
            [[RLMRealm defaultRealm] addObject:subcategoryRealm];
        }];
    }
}

+ (void)saveLocations:(NSMutableArray *)json{
    for (NSDictionary * location in json) {
        LocationModel * locationRealm = [[LocationModel alloc] initWithValue:@{
                                                                               @"idLocation" : [location valueForKey:@"id"] ?: @"",
                                                                               @"nameLocation" : [location valueForKey:@"name"] ?: @"",
                                                                               @"zip" : [location valueForKey:@"zip"] ?: @"",
                                                                               @"geoLat" : [location valueForKey:@"geo_lat"] ?: @"",
                                                                               @"geoLng" : [location valueForKey:@"geo_lng"] ?: @""
                                                                               }];
        [[RLMRealm defaultRealm] transactionWithBlock:^{
            [[RLMRealm defaultRealm] addObject:locationRealm];
        }];
    }
}

@end
