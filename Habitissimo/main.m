//
//  main.m
//  habitissimo
//
//  Created by Patri on 06/08/2019.
//  Copyright © 2019 patri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
