//
//  RequestsViewController.m
//  Habitissimo
//
//  Created by Patri on 08/08/2019.
//  Copyright © 2019 patri. All rights reserved.
//

#import "RequestsViewController.h"
#import "RequestModel.h"
#import "DataService.h"
#import "RequestCell.h"
#import "SCLAlertView.h"

@interface RequestsViewController ()

@property (nonatomic, strong) RLMResults *resultRealmRequests;  // Property to store Realm query results
@property (weak, nonatomic) IBOutlet UIBarButtonItem *buttonForm;   // New budget request button

@end

@implementation RequestsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Setup table
    self.tableRequests.dataSource = self;
    self.tableRequests.delegate = self;
    self.tableRequests.tableFooterView = [UIView new]; // Remove unused cells' borders
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    if (![DataService isInitialized]) {
        // Disable the button for new request until the category information is fetched
        [self.buttonForm setEnabled:FALSE];

        [self initializeDatabase];
    }

    // Refresh table data before appearing
    [self refreshTableData];
}

#pragma mark Data initialization and table reload

- (void)refreshTableData {
    self.resultRealmRequests = [DataService allRequests];
    [self.tableRequests reloadData];
}

- (void)initializeDatabase {
    // Fetch data from endpoints
    [DataService loadInitialData:^(BOOL resultOk) {
        if (!resultOk) {
            SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
            alert.backgroundType = SCLAlertViewBackgroundBlur;
            [alert addButton:@"Volver a intentar" actionBlock:^(void) {
                [self initializeDatabase];
            }];
            [alert showNotice:@"Ups!" subTitle:@"Parece que ha habido un problema con la carga de los datos" closeButtonTitle:@"Cerrar" duration:0.0f];
        } else {
            [self.buttonForm setEnabled:TRUE];
            [self.tableView reloadData];
        }
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.resultRealmRequests.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * CellIdentifier = @"requestCell";
    RequestCell * cell = (RequestCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[RequestCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    RequestModel * request = [self.resultRealmRequests objectAtIndex:indexPath.row];
    cell.lblName.text = request.nameRequest;
    cell.lblSubcategory.text = request.subcategory;
    cell.lblEmail.text = request.email;
    cell.lblPhone.text = request.phone;
    cell.lblLocation.text = request.location;
    cell.lblDescription.text = request.descriptionRequest;

    return cell;
}

@end
