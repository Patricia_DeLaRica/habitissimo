//
//  RequestsViewController.h
//  Habitissimo
//
//  Created by Patri on 08/08/2019.
//  Copyright © 2019 patri. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RequestsViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UITableView *tableRequests;

@end

NS_ASSUME_NONNULL_END
