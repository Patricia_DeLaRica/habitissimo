//
//  AddRequestViewController.m
//  Habitissimo
//
//  Created by Patri on 06/08/2019.
//  Copyright © 2019 patri. All rights reserved.
//

#import "AddRequestViewController.h"
#import <RMPickerViewController/RMPickerViewController.h>
#import "DataService.h"
#import "CategoryModel.h"
#import "SubcategoryModel.h"
#import "LocationModel.h"
#import <UITextField_AutoSuggestion/UITextField+AutoSuggestion.h>
#import "SCLAlertView.h"
#import "RequestModel.h"
#import <IQKeyboardManager.h>
#import "Validator.h"

@interface AddRequestViewController ()<UITextFieldAutoSuggestionDataSource, UIPickerViewDelegate, UIPickerViewDataSource>

// IBOutlets

@property (weak, nonatomic) IBOutlet UIButton *pickerCategories;
@property (weak, nonatomic) IBOutlet UIButton *pickerSubCategories;
@property (weak, nonatomic) IBOutlet UIButton *buttonSendRequest;

@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPhone;
@property (weak, nonatomic) IBOutlet UITextField *txtLocation;
@property (weak, nonatomic) IBOutlet UITextView *txtDescription;

// Business properties

@property RLMResults<CategoryModel *> *resultRealmCategories;
@property RLMResults<SubcategoryModel *> *resultRealmSubCategories;
//@property RLMResults<LocationModel *> *resultRealmLocation;

@property NSArray *stringsComplete;
@property CategoryModel *categorySelected;
@property SubcategoryModel *subCategorySelected;

@end

@implementation AddRequestViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.txtLocation.autoSuggestionDataSource = self;
    self.txtLocation.showImmediately = false;
    [self.txtLocation observeTextFieldChanges];
    
    // Dismiss keyboard helper when tapping outside textfields
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self loadAppearance];
    [self loadCategories];
    self.stringsComplete = [DataService locationAutocompleteStrings];

}

#pragma mark Appearance, keyboard and initialization methods

- (void)loadAppearance {
    [self customizeStyleTexFormLayer:self.txtName.layer];
    [self customizeStyleTexFormLayer:self.txtEmail.layer];
    [self customizeStyleTexFormLayer:self.txtPhone.layer];
    [self customizeStyleTexFormLayer:self.txtLocation.layer];
    [self customizeStyleTexFormLayer:self.txtDescription.layer];
    self.buttonSendRequest.layer.cornerRadius = 5.0;
}

- (void)customizeStyleTexFormLayer:(CALayer *)viewLayer {
    UIColor *borderColor = [UIColor colorWithRed:0.93 green:0.48 blue:0.19 alpha:1.0];
    viewLayer.borderColor = borderColor.CGColor;
    viewLayer.borderWidth = 1.0;
    viewLayer.cornerRadius = 5.0;
}

- (void)dismissKeyboard {
    [self.txtName resignFirstResponder];
    [self.txtEmail resignFirstResponder];
    [self.txtPhone resignFirstResponder];
    [self.txtLocation resignFirstResponder];
    [self.txtDescription resignFirstResponder];
}

- (void)loadCategories {
    self.resultRealmCategories = [DataService allCategories];
    self.categorySelected = [self.resultRealmCategories objectAtIndex:0];
    [self.pickerCategories setTitle:self.categorySelected.nameCategory forState:UIControlStateNormal];
    [self loadSubCategoriesWitIdCat:self.categorySelected.idCategory];
}

- (void)loadSubCategoriesWitIdCat:(NSString *)idCategory {
    self.resultRealmSubCategories= [DataService allSubcategoriesForCategoryId:idCategory];
    self.subCategorySelected = [self.resultRealmSubCategories objectAtIndex:0];
    [self.pickerSubCategories setTitle:self.subCategorySelected.nameSubcategory forState:UIControlStateNormal];
}

#pragma mark AutoSuggestField delegate methods

- (UITableViewCell *)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text{
    
    static NSString *cellIdentifier = @"LocationAutoSuggestionCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSArray *locations = self.stringsComplete;
    if (text.length > 0) {
        NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@", text];
        locations = [self.stringsComplete filteredArrayUsingPredicate:filterPredictate];
    }
    cell.textLabel.text = locations[indexPath.row];
    
    return cell;
}

- (NSInteger)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section forText:(NSString *)text {
    // All values when no character is entered
    if (text.length == 0) {
        return self.stringsComplete.count;
    }
    NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@", text];
    NSInteger count = [self.stringsComplete filteredArrayUsingPredicate:filterPredictate].count;
    return count;
}

- (void)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
    NSLog(@"Selected suggestion at index row - %ld", (long)indexPath.row);
    
    NSArray *locations = self.stringsComplete;
    if (text.length > 0) {
        NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@", text];
        locations = [self.stringsComplete filteredArrayUsingPredicate:filterPredictate];
    }
    self.txtLocation.text = locations[indexPath.row];
}

- (CGFloat)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
    return 50;
}

#pragma mark Categories and Subcategories picker methods

- (IBAction)openCategoriesPicker:(id)sender {
    
    RMAction<UIPickerView *> *selectAction = [RMAction<UIPickerView *> actionWithTitle:@"Seleccionar" style:RMActionStyleDone andHandler:^(RMActionController<UIPickerView *> *controller) {
        
        NSNumber *selectedRowtmp = @([controller.contentView selectedRowInComponent:0]);
        NSInteger selectedRow = [selectedRowtmp integerValue];
        self.categorySelected = [self.resultRealmCategories objectAtIndex:selectedRow];
        [self.pickerCategories setTitle: self.categorySelected.nameCategory forState: UIControlStateNormal];
        self.categorySelected = [self.resultRealmCategories objectAtIndex:selectedRow];
        [self loadSubCategoriesWitIdCat:[[self.resultRealmCategories objectAtIndex:selectedRow] idCategory]];
    }];
    
    RMAction<UIPickerView *> *cancelAction = [RMAction<UIPickerView *> actionWithTitle:@"Cancelar" style:RMActionStyleCancel andHandler:^(RMActionController<UIPickerView *> *controller) {
        NSLog(@"Row selection was canceled");
    }];
    
    RMPickerViewController *pickerController = [RMPickerViewController actionControllerWithStyle:RMActionControllerStyleDefault title:@"Categoría" message:@"Selecciona una categoría de la lista" selectAction:selectAction andCancelAction:cancelAction];
    pickerController.picker.tag = 0;
    pickerController.picker.dataSource = self;
    pickerController.picker.delegate = self;
    [self presentViewController:pickerController animated:YES completion:nil];
}

- (IBAction)openPickerSubCategories:(id)sender {
    RMAction<UIPickerView *> *selectAction = [RMAction<UIPickerView *> actionWithTitle:@"Seleccionar" style:RMActionStyleDone andHandler:^(RMActionController<UIPickerView *> *controller) {
        
        NSNumber *selectedRowtmp = @([controller.contentView selectedRowInComponent:0]);
        NSInteger selectedRow = [selectedRowtmp integerValue];
        
        self.subCategorySelected = [self.resultRealmSubCategories objectAtIndex:selectedRow];
        [self.pickerSubCategories setTitle: self.subCategorySelected.nameSubcategory forState: UIControlStateNormal];
    }];
    
    RMAction<UIPickerView *> *cancelAction = [RMAction<UIPickerView *> actionWithTitle:@"Cancelar" style:RMActionStyleCancel andHandler:^(RMActionController<UIPickerView *> *controller) {
        NSLog(@"Row selection was canceled");
    }];
    
    RMPickerViewController *pickerController = [RMPickerViewController actionControllerWithStyle:RMActionControllerStyleDefault title:@"Subcategoría" message:@"Selecciona una subcategoria de la lista" selectAction:selectAction andCancelAction:cancelAction];
    pickerController.picker.tag = 1;
    pickerController.picker.dataSource = self;
    pickerController.picker.delegate = self;
    
    [self presentViewController:pickerController animated:YES completion:nil];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    // Using component tag for quick identify the picker (0-Categories, 1-Subcategories)
    if (pickerView.tag == 0) {
        return self.resultRealmCategories.count;
    } else {
        return self.resultRealmSubCategories.count;
    }
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    // Using component tag for quick identify the picker (0-Categories, 1-Subcategories)
    if (pickerView.tag == 0) {
        return [[self.resultRealmCategories objectAtIndex:row] nameCategory];
    } else {
        return [[self.resultRealmSubCategories objectAtIndex:row] nameSubcategory];
    }
}

#pragma mark Send button action

- (IBAction)clickedSendRequest:(id)sender {
    
    SCLAlertView *alert = [[SCLAlertView alloc] init];

    if ([self checkMandatoryFields]) {
        if ([Validator validatePhone:self.txtPhone.text]) {
            if ([Validator validateEmailWithString:self.txtEmail.text]) {
                if ([Validator validateLocation:self.txtLocation.text]) {
                    
                    NSLog(@"Formulario de presupuesto OK");
                    
                    [DataService saveRequestWithName:self.txtName.text email:self.txtEmail.text phone:_txtPhone.text category:self.pickerCategories.titleLabel.text location:self.txtLocation.text subcategory:self.pickerSubCategories.titleLabel.text description:self.txtDescription.text];
                    
                    alert.backgroundType = SCLAlertViewBackgroundBlur;
                    [alert alertDismissAnimationIsCompleted:^{
                        [self.navigationController popViewControllerAnimated:TRUE];
                    }];
                    [alert showSuccess:self title:@"OK!" subTitle:@"La petición de presupuesto ha sido correctamente enviada." closeButtonTitle:@"Aceptar" duration:0.0f];
                    
                } else { // Location validation failed
                    alert.backgroundType = SCLAlertViewBackgroundBlur;
                    [alert showNotice:self title:@"Ups!" subTitle:@"La localización introducida no es correcta :(" closeButtonTitle:@"OK" duration:0.0f];
                }
            } else { // Email validation failed
                alert.backgroundType = SCLAlertViewBackgroundBlur;
                [alert showNotice:self title:@"Ups!" subTitle:@"El formato del email no es correcto. ej: user@hola.com" closeButtonTitle:@"OK" duration:0.0f];
            }
        } else { // Phone format validation failed
            alert.backgroundType = SCLAlertViewBackgroundBlur;
            [alert showNotice:self title:@"Ups!" subTitle:@"El formato del telefono no es correcto. ej: +34123456789" closeButtonTitle:@"OK" duration:0.0f];
        }
    } else { // Some field is empty
        alert.backgroundType = SCLAlertViewBackgroundBlur;
        [alert showNotice:self title:@"Ups!" subTitle:@"No has rellenado todos los campos del formulario :(" closeButtonTitle:@"OK" duration:0.0f];
    }
}

- (BOOL)checkMandatoryFields {
    return [Validator validateStringsNotEmptyInArray:@[self.txtName.text, self.txtEmail.text, self.txtPhone.text, self.txtLocation.text, self.txtDescription.text]];
}

@end
