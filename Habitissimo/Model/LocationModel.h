//
//  LocationModel.h
//  Habitissimo
//
//  Created by Patri on 07/08/2019.
//  Copyright © 2019 patri. All rights reserved.
//

#import "RLMObject.h"


NS_ASSUME_NONNULL_BEGIN

@interface LocationModel : RLMObject

@property int idLocation;
@property NSString *parentId;
@property NSString *nameLocation;
@property NSString *zip;
@property double geoLat;
@property double geoLng;
@property NSString *level;
@property NSString *slug;

@end

NS_ASSUME_NONNULL_END
