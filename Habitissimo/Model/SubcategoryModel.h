//
//  SubcategoryModel.h
//  Habitissimo
//
//  Created by Patri on 09/08/2019.
//  Copyright © 2019 patri. All rights reserved.
//

#import "RLMObject.h"
#import "CategoryModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SubcategoryModel : RLMObject
    
@property NSString *idSubcategory;
@property NSString *nameSubcategory;
@property NSString *idparent;

@end

NS_ASSUME_NONNULL_END
