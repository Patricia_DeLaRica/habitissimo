//
//  RequestModel.h
//  Habitissimo
//
//  Created by Patri on 08/08/2019.
//  Copyright © 2019 patri. All rights reserved.
//

#import "RLMObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface RequestModel : RLMObject

@property NSString *nameRequest;
@property NSString *email;
@property NSString *phone;
@property NSString *location;
@property NSString *category;
@property NSString *subcategory;
@property NSString *descriptionRequest;

@end

NS_ASSUME_NONNULL_END
