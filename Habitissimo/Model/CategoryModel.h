//
//  CategoryModel.h
//  Habitissimo
//
//  Created by Patri on 07/08/2019.
//  Copyright © 2019 patri. All rights reserved.
//

#import "RLMObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface CategoryModel : RLMObject

@property NSString *idCategory;
@property NSString *nameCategory;


@end

NS_ASSUME_NONNULL_END
