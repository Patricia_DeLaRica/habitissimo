//
//  AppDelegate.h
//  habitissimo
//
//  Created by Patri on 06/08/2019.
//  Copyright © 2019 patri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

