//
//  HabitissimoDataServiceTests.m
//  HabitissimoDataServiceTests
//
//  Created by Patri on 09/08/2019.
//  Copyright © 2019 patri. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "../Habitissimo/Service/DataService.h"
#import "../Habitissimo/Model/RequestModel.h"
#import <OHHTTPStubs.h>

// Dataservice testing scope extension
@interface DataService (Tests)
+ (BOOL)fetchAndSaveCategories;
+ (BOOL)fetchAndSaveSubCategoriesForCategoryId:(NSString *)idCategory;
+ (BOOL)fetchAndSaveLocations;
@end


@interface HabitissimoDataServiceTests : XCTestCase

@end

@implementation HabitissimoDataServiceTests

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testNetworkFailDetectionWhenDataInitialization {
    // Create a network failing connection stub
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * _Nonnull request) {
        return YES;
    } withStubResponse:^OHHTTPStubsResponse * _Nonnull(NSURLRequest * _Nonnull request) {
        NSError* notConnectedError = [NSError errorWithDomain:NSURLErrorDomain code:kCFURLErrorNotConnectedToInternet userInfo:nil];
        return [OHHTTPStubsResponse responseWithError:notConnectedError];
    }];
    
    XCTAssertFalse([DataService fetchAndSaveCategories]);
    XCTAssertFalse([DataService fetchAndSaveLocations]);
    XCTAssertFalse([DataService fetchAndSaveSubCategoriesForCategoryId:@""]);

    [OHHTTPStubs removeAllStubs];
}

- (void)testRequestElementCreation {
    // Given
    NSString *testUserName = @"John Doe Testing Name - 123456789";
    
    RequestModel *savedModel = (RequestModel *)[DataService saveRequestWithName:testUserName
                                                                          email:@"john@doe.com"
                                                                          phone:@"+34900123123"
                                                                       category:@"Construccion"
                                                                       location:@"España"
                                                                    subcategory:@"Construccion de casas"
                                                                    description:@"Necesito construir una casa"];
    
    RequestModel *fetchedRequest = [[DataService allRequestsByName:testUserName] count] > 0 ? [DataService allRequestsByName:testUserName][0] : nil;
    
    XCTAssertNotNil (fetchedRequest);
    
    XCTAssertTrue   ([savedModel.nameRequest isEqualToString:fetchedRequest.nameRequest]);
    XCTAssertTrue   ([savedModel.email isEqualToString:fetchedRequest.email]);
    XCTAssertTrue   ([savedModel.phone isEqualToString:fetchedRequest.phone]);
    XCTAssertTrue   ([savedModel.category isEqualToString:fetchedRequest.category]);
    XCTAssertTrue   ([savedModel.subcategory isEqualToString:fetchedRequest.subcategory]);
    XCTAssertTrue   ([savedModel.location isEqualToString:fetchedRequest.location]);
    XCTAssertTrue   ([savedModel.description isEqualToString:fetchedRequest.description]);

    // Remove testing objects
    [[RLMRealm defaultRealm] beginWriteTransaction];
    [[RLMRealm defaultRealm] deleteObjects:[DataService allRequestsByName:testUserName]];
    [[RLMRealm defaultRealm] commitWriteTransaction];
}

@end
