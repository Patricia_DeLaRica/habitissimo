//
//  HabitissimoValidationTests.m
//  HabitissimoValidationTests
//
//  Created by Patri on 09/08/2019.
//  Copyright © 2019 patri. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "../Habitissimo/Util/Validator.h"

@interface HabitissimoValidationTests : XCTestCase

@end

@implementation HabitissimoValidationTests

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testValidatorPhoneValidation {
    XCTAssertFalse ([Validator validatePhone:nil]);
    XCTAssertFalse ([Validator validatePhone:@""]);
    XCTAssertFalse ([Validator validatePhone:@"1234"]);
    XCTAssertFalse ([Validator validatePhone:@"+34"]);
    XCTAssertFalse ([Validator validatePhone:@"34123456789"]);
    XCTAssertFalse ([Validator validatePhone:@"abc"]);
    XCTAssertFalse ([Validator validatePhone:@"a34123456789"]);
    
    XCTAssertTrue  ([Validator validatePhone:@"+34123456789"]);
    XCTAssertTrue  ([Validator validatePhone:@"0034123456789"]);
}

- (void)testValidatorEmailValidation {
    XCTAssertFalse ([Validator validateEmailWithString:nil]);
    XCTAssertFalse ([Validator validateEmailWithString:@""]);
    XCTAssertFalse ([Validator validateEmailWithString:@"asdfasdf"]);
    XCTAssertFalse ([Validator validateEmailWithString:@"asdfasdf.com"]);
    XCTAssertFalse ([Validator validateEmailWithString:@"asdfasdf@asdfasdf"]);
    XCTAssertFalse ([Validator validateEmailWithString:@"asdfasdf@asdfasdf."]);
    
    XCTAssertTrue  ([Validator validateEmailWithString:@"asdfasdf@asdfasdf.com"]);
}

- (void)testValidatorNotEmptyStringsValidation {

    // Given
    NSArray *nilArray = nil;
    NSArray *invalidArrayOfNotEmptyStrings = @[ @"", @"a" ];
    NSArray *anotherInvalidArrayOfNotEmptyStrings = @[ @"a", @"" ];
    NSArray *validArrayOfNotEmptyStrings = @[ @"a", @"b", @"c" ];

    // Then fail when
    XCTAssertFalse ([Validator validateStringsNotEmptyInArray:nilArray]);
    XCTAssertFalse ([Validator validateStringsNotEmptyInArray:@[]]);
    XCTAssertFalse ([Validator validateStringsNotEmptyInArray:@[@""]]);
    XCTAssertFalse ([Validator validateStringsNotEmptyInArray:invalidArrayOfNotEmptyStrings]);
    XCTAssertFalse ([Validator validateStringsNotEmptyInArray:anotherInvalidArrayOfNotEmptyStrings]);
    
    // And success when
    XCTAssertTrue  ([Validator validateStringsNotEmptyInArray:@[@"asdf"]]);
    XCTAssertTrue  ([Validator validateStringsNotEmptyInArray:validArrayOfNotEmptyStrings]);
}

@end
